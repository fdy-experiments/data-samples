# data-samples

Data samples folders with source data of different kind : XML, JSON, CSV, with for each at least : 
* input sample
* mapping either in RML or merely as "commented target RDF"
* output RDF

And when possible
* source data model or documentation

